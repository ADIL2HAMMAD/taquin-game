

        /*  Position For Taquin 3*3 ==> 9 Position available */
        pos3 = [
            {
                "row" : "line N 1",
                "x":  gamebg.x  + 190,
                "y":  gamebg.y - 170,
            },
            {
                "x": gamebg.x + 360,
                "y":  gamebg.y - 170,
            },
            {
                "x":  gamebg.x + 530 ,
                "y":  gamebg.y - 170,
            },
            {
                "row" : "line N 2",
                "x":  gamebg.x  + 190,
                "y": gamebg.y + 0 ,
            },
            {
                "x": gamebg.x + 360,
                "y": gamebg.y + 0 ,
            },
            {
                "x":  gamebg.x + 530 ,
                "y": gamebg.y + 0 ,
            },
            {
                "row" : "line N 3",
                "x":  gamebg.x  + 190,
                "y":  gamebg.y + 170 ,
            },
            {
                "x": gamebg.x + 360,
                "y":  gamebg.y + 170 ,
            },
            {
                "x":  gamebg.x + 530 ,
                "y":  gamebg.y + 170 ,
            },

        ]


        /*  Position For Taquin 4*4 ==> 16 Position available */
        pos4 = [
            {
                "row" : "line N 1",
                "x": gamebg.x + 145,
                "y":  gamebg.y - 225,
            },
            {
                "x": gamebg.x + 295,
                "y":  gamebg.y - 225,
            },
            {
                "x":  gamebg.x + 445 ,
                "y":  gamebg.y - 225,
            },
            {
                "x":  gamebg.x + 595,
                "y":  gamebg.y - 225,
            },
            {
                "row" : "line N 2",
                "x": gamebg.x + 145,
                "y": gamebg.y - 75 ,
            },
            {
                "x": gamebg.x + 295,
                "y": gamebg.y - 75 ,
            },
            {
                "x":  gamebg.x + 445 ,
                "y": gamebg.y - 75 ,
            },
            {
                "x":  gamebg.x + 595,
                "y": gamebg.y - 75 ,
            },
            {
                "row" : "line N 3",
                "x": gamebg.x + 145,
                "y": gamebg.y  + 75 ,
            },
            {
                "x": gamebg.x + 295,
                "y": gamebg.y  + 75 ,
            },
            {
                "x":  gamebg.x + 445 ,
                "y": gamebg.y  + 75 ,
            },
            {
                "x":  gamebg.x + 595,
                "y": gamebg.y  + 75 ,
            },
            {
                "row" : "line N 4",
                "x": gamebg.x + 145,
                "y": gamebg.y + 225 ,
            },
            {
                "x": gamebg.x + 295,
                "y": gamebg.y + 225 ,
            },
            {
                "x":  gamebg.x + 445 ,
                "y": gamebg.y + 225 ,
            },
            {
                "x":  gamebg.x + 595,
                "y": gamebg.y + 225 ,
            }
        ]


        /*  Position For Taquin 4*4 ==> 16 Position available */
        pos5 = [
            {
                "row" : "line N 1",
                "x": gamebg.x  + 110,
                "y":  gamebg.y - 260,
            },
            {
                "x": gamebg.x + 240,
                "y":  gamebg.y - 260,
            },
            {
                "x":  gamebg.x + 370 ,
                "y":  gamebg.y - 260,
            },
            {
                "x":  gamebg.x + 500,
                "y":  gamebg.y - 260,
            },
            {
                "x": gamebg.x + 630,
                "y":  gamebg.y - 260,
            },


            {
                "row" : "line N 2",
                "x": gamebg.x  + 110,
                "y":  gamebg.y - 130 ,
            },
            {
                "x": gamebg.x + 240,
                "y":  gamebg.y - 130 ,
            },
            {
                "x":  gamebg.x + 370 ,
                "y":  gamebg.y - 130 ,
            },
            {
                "x":  gamebg.x + 500,
                "y":  gamebg.y - 130 ,
            },
            {
                "x": gamebg.x + 630,
                "y":  gamebg.y - 130 ,
            },



            {
                "row" : "line N 3",
                "x": gamebg.x  + 110,
                "y": gamebg.y + 0 ,
            },
            {
                "x": gamebg.x + 240,
                "y": gamebg.y + 0 ,
            },
            {
                "x":  gamebg.x + 370 ,
                "y": gamebg.y + 0 ,
            },
            {
                "x":  gamebg.x + 500,
                "y": gamebg.y + 0 ,
            },
            {
                "x": gamebg.x + 630,
                "y": gamebg.y + 0 ,
            },



            {
                "row" : "line N 4",
                "x": gamebg.x  + 110,
                "y": gamebg.y + 130 ,
            },
            {
                "x": gamebg.x + 240,
                "y": gamebg.y + 130 ,
            },
            {
                "x":  gamebg.x + 370 ,
                "y": gamebg.y + 130 ,
            },
            {
                "x":  gamebg.x + 500,
                "y": gamebg.y + 130 ,
            },
            {
                "x": gamebg.x + 630,
                "y": gamebg.y + 130 ,
            },



            {
                "row" : "line N 5",
                "x": gamebg.x  + 110,
                "y": gamebg.y + 260 ,
            },
            {
                "x": gamebg.x + 240,
                "y": gamebg.y + 260 ,
            },
            {
                "x":  gamebg.x + 370 ,
                "y": gamebg.y + 260 ,
            },
            {
                "x":  gamebg.x + 500,
                "y": gamebg.y + 260 ,
            },
            {
                "x": gamebg.x + 630,
                "y": gamebg.y + 260 ,
            },
        ]




        /* Fill NineAleatoires Array with Aleatoire poisition */
        NineAleatoires = [];
        var posRestants = pos3.slice(0);
        while (NineAleatoires.length < 9) {
            NineAleatoires.push(posRestants.splice(Math.floor(Math.random() * posRestants.length), 1)[0]);
        }


        /* Fill SexteenAleatoires Array with Aleatoire poisition */
        SexteenAleatoires = [];
        var posRestants = pos4.slice(0);
        while (SexteenAleatoires.length < 16) {
            SexteenAleatoires.push(posRestants.splice(Math.floor(Math.random() * posRestants.length), 1)[0]);
        }


        /* Fill TweentyfiveAleatoires Array with Aleatoire poisition */
        TweentyfiveAleatoires = [];
        var posRestants = pos5.slice(0);
        while (TweentyfiveAleatoires.length < 25) {
            TweentyfiveAleatoires.push(posRestants.splice(Math.floor(Math.random() * posRestants.length), 1)[0]);
        }


        // sprite = game.add.sprite(gamebg.x, gamebg.y, "puzzle")
        // sprite.anchor.set( 0.5)



        /* the 9 images Defined Aleatoire
        this.DisplaySprits(9,3) */

        /* the 16 images Defined Aleatoire
        this.DisplaySprits(16,4)*/

        /* the 25 images Defined Aleatoire
        this.DisplaySprits(25,5)*/




    DisplaySprits:function(spritsNb , rows ){
        var emptySprite =   "t"+ rows +"pblack"
        var  postionArray = [];
        if (rows == 3) {
            postionArray = NineAleatoires
        }
        if(rows == 4){
            postionArray = SexteenAleatoires
        }
        if(rows == 5){
            postionArray = TweentyfiveAleatoires
        }

        for (i = 0; i < spritsNb - 1; i++) {
            var nameSprit = `t${rows}p${i+1}`
            nameSprit = game.add.sprite(postionArray[i].x, postionArray[i].y, nameSprit)
            nameSprit.anchor.set( 0.5)
        }
        emptySprite = game.add.sprite(postionArray[spritsNb - 1].x, postionArray[spritsNb - 1].y, emptySprite)
        emptySprite.black = true;
        emptySprite.anchor.set( 0.5)
    }





    // Lvl 3 5*5

    var level3 = {

        create: function() {
            langue = localStorage.getItem('langue')
            me = this
            shuffledIndexArray = []
            finished = false
            second = 0
            minute = 0
            hours  = 0

            //Title Arabic and Latin Font Style
            var titlestyle = { font: '70px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" };
            var titlestyleAR = { font: '70px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" };
            // Arabic and Latin Font Style
            var noticestyle = { font: '50px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };
            var noticestyleAR = { font: '50px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };

            /* Game background */
            bg = game.add.sprite(game.width / 2, game.height / 2, 'taqinbg')
            bg.anchor.set(0.5, 0.5)

            /* SideBar background */
            sidebarbg = game.add.sprite(game.width , game.height / 2, 'sidebarbg')
            sidebarbg.anchor.set(1, 0.5)

            //SideBar Message -Notice-
            notice = game.add.text(sidebarbg.x - 240, sidebarbg.y   , " ", noticestyle);
            notice.anchor.set(0.5)
            noticeAR = game.add.text(sidebarbg.x - 240, sidebarbg.y   , " ", noticestyleAR);
            noticeAR.anchor.set(0.5)

            if (langue == 'fr') {
                notice.text = "Merci de faire Glisser les pièces afin de gagner" ;
            }if (langue == 'en') {
                notice.text = "Thanks for dragging coins to win" ;
            }else if (langue == 'ar'){
                noticeAR.text = "شكرا لسحب العملات المعدنية للفوز" ;
            }

            /* Flags  */
            ma_flag = game.add.sprite(sidebarbg.x - 450 , sidebarbg.y + 450  , 'ma_flag')
            ma_flag.anchor.set(0)
            ma_flag.scale.setTo(0.2);

            fr_flag = game.add.sprite(sidebarbg.x - 300, sidebarbg.y  + 450 , 'fr_flag')
            fr_flag.anchor.set(0)
            fr_flag.scale.setTo(0.2);

            en_flag = game.add.sprite(sidebarbg.x  - 150 , sidebarbg.y  + 450 , 'en_flag')
            en_flag.anchor.set(0)
            en_flag.scale.setTo(0.2);

            title = game.add.text(sidebarbg.x - 400 , sidebarbg.y - 525 , " ", titlestyle);
            titleAR = game.add.text(sidebarbg.x - 350 , sidebarbg.y - 525 , " ", titlestyleAR);

            if (langue == 'fr') {
                title.text = "Jeu de Taquin" ;
            }if (langue == 'en') {
                title.text = "Teaser Game" ;
            }else if (langue == 'ar'){
                titleAR.text = "لعبة اللغز" ;
            }


            /* Puzzle White Background Elements */
            gamebg = game.add.sprite( 300  ,  game.height / 2 - 430, 'gamebg')
            gamebg.anchor.set(0)
            gamebg.scale.setTo(1.2)



            ma_flag.inputEnabled = true;
            ma_flag.events.onInputDown.add(function() {
                this.ChangeLanguage('ar');
            }, this);
            fr_flag.inputEnabled = true;
            fr_flag.events.onInputDown.add(function() {
                this.ChangeLanguage('fr');
            }, this);
            en_flag.inputEnabled = true;
            en_flag.events.onInputDown.add(function() {
                this.ChangeLanguage('en');
            }, this);

            this.prepareBoard()

            gameUIenter()
        },

        prepareBoard: function() {

            var piecesIndex = 0,
            i=0 ;
            j=0 ;
            piece = null;
            PIECE_WIDTH = 150;
            PIECE_HEIGHT = PIECE_WIDTH;

            PADDING_TOP = 0;
            PADDING_LEFT = 0;

            BOARD_COLS = 5;
            BOARD_ROWS = 5;

            piecesAmount = BOARD_COLS * BOARD_ROWS;

            shuffledIndexArray = this.createShuffledIndexArray();

            piecesGroup = game.add.group();

            for (i = 0; i < BOARD_ROWS; i++)
            {

                for (j = 0; j < BOARD_COLS; j++)
                {

                    if (shuffledIndexArray[piecesIndex]) {
                        piece = piecesGroup.create( (gamebg.x + 65) + j * PIECE_WIDTH ,(gamebg.y + 65) +  i * PIECE_HEIGHT , "puzzlelvl3", shuffledIndexArray[piecesIndex]);
                    }
                    else{ //initial position of black piece
                        piece = piecesGroup.create((gamebg.x + 65) +  j * PIECE_WIDTH , (gamebg.y + 65 ) +  i * PIECE_HEIGHT);
                        piece.black = true;
                    }
                    piece.anchor.set(0)
                    piece.name = 'piece' + i.toString() + 'x' + j.toString();
                    piece.currentIndex = piecesIndex;
                    piece.destIndex =  shuffledIndexArray[piecesIndex]; // shuffledIndexArray[piecesIndex] ==> Frame ID from Source Image and Final distination frame ID
                    piece.inputEnabled = true;
                    piece.events.onInputDown.add(this.selectPiece, this);
                    piece.posX = j;
                    piece.posY = i;
                    piecesIndex++;

                }
            }


            counterbg = game.add.sprite(fr_flag.x + 50 , fr_flag.y  - 95, 'counterbg')
            counterbg.anchor.set(0.5)
            counterbg.scale.setTo(0.8)

            counter = game.add.text(fr_flag.x , fr_flag.y  - 120, '0 : 0 : 0', { font: "40px NotoSansExtraCondensedLight", fill: "#ffffff", align: "center" , fontWeight : "bold" });
            game.time.events.loop(Phaser.Timer.SECOND, this.updateCounter, this);

        },

        updateCounter:function() {
            if (!finished) {
                second++ ;
                if (second == 60) {
                    second = 0
                    minute++
                }
                if (minute == 60) {
                    second = 0
                    minute = 0
                    hours++
                }
                counter.setText(hours + ' : ' + minute + ' : ' + second);
            }else{
                piecesGroup.children.forEach(function(element) {
                    element.alpha = 0;
                });

                fullpuzzlelvl3 = game.add.sprite( gamebg.x  + 440 , gamebg.y  + 440 , 'fullpuzzlelvl3')
                fullpuzzlelvl3.anchor.set(0.5)

                game.add.tween(fullpuzzlelvl3.scale).to({ x: 1.05, y: 1.05 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                    game.add.tween(fullpuzzlelvl3.scale).to({ x: 1, y: 1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                    }, this);
                }, this);

                counter.alpha = 1;
                game.add.tween(counter).to({ alpha: 0.5 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                    game.add.tween(counter).to({ alpha: 1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                    }, this);
                }, this);


            }

        },

        selectPiece: function(piece) {

            var blackPiece = this.canMove(piece);

            //if there is a black piece in neighborhood
            if (blackPiece) {
                this.movePiece(piece, blackPiece);
            }

        },

        canMove: function(piece) {

            var foundBlackElem = false;

            piecesGroup.children.forEach(function(element) {
                if (element.posX === (piece.posX - 1) && element.posY === piece.posY && element.black ||
                element.posX === (piece.posX + 1) && element.posY === piece.posY && element.black ||
                element.posY === (piece.posY - 1) && element.posX === piece.posX && element.black ||
                element.posY === (piece.posY + 1) && element.posX === piece.posX && element.black) {
                    foundBlackElem = element;
                    return;
                }
            });

            return foundBlackElem;
        },


        movePiece: function(piece, blackPiece) {

            var tmpPiece = {
                posX: piece.posX,
                posY: piece.posY,
                currentIndex: piece.currentIndex
            };

            game.add.tween(piece).to({x: (gamebg.x + 65) + blackPiece.posX * PIECE_WIDTH, y: (gamebg.y + 65) + blackPiece.posY * PIECE_HEIGHT}, 300, Phaser.Easing.Linear.None, true);

            //change places of piece and blackPiece
            piece.posX = blackPiece.posX;
            piece.posY = blackPiece.posY;
            piece.currentIndex = blackPiece.currentIndex;
            piece.name ='piece' + piece.posX.toString() + 'x' + piece.posY.toString();

            //piece is the new black
            blackPiece.posX = tmpPiece.posX;
            blackPiece.posY = tmpPiece.posY;
            blackPiece.currentIndex = tmpPiece.currentIndex;
            blackPiece.name ='piece' + blackPiece.posX.toString() + 'x' + blackPiece.posY.toString();

            //after every move check if puzzle is completed
            this.checkIfFinished();
        },
        checkIfFinished : function() {
            var isFinished = true;

            piecesGroup.children.forEach(function(element) {
                element.destIndex = 1;
                element.currentIndex =1 ;

                if (element.currentIndex !== element.destIndex) {
                    isFinished = false;
                    return;
                }
            });

            if (isFinished) {
                //Piece With Description at Finished
                notice.alpha = 0
                noticeAR.alpha = 0

                if (langue == 'fr') {
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceFR')
                }if (langue == 'en') {
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceEN')
                }else if (langue == 'ar'){
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceAR')
                }

                lvl3pieceWithDesc.anchor.set(1, 0.5)
                finished = true;
            }

        },
        createShuffledIndexArray: function() {

            var indexArray = [];

            for (var i = 0; i < piecesAmount; i++)
            {
                indexArray.push(i);
            }

            return this.shuffle(indexArray);

        },

        shuffle: function(array) {

            var counter = array.length;
            temp = null;
            index = 0;

            while (counter > 0)
            {
                index = Math.floor(Math.random() * counter);

                counter--;

                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }

            return array;

        },
        ChangeLanguage : function(langue ){
            localStorage.setItem('langue' , langue);

            if (finished) {
                if (langue == 'fr') {
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceFR')
                }if (langue == 'en') {
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceEN')
                }else if (langue == 'ar'){
                    lvl3pieceWithDesc = game.add.sprite(sidebarbg.x-20, sidebarbg.y - 40, 'lvl3pieceAR')
                }
                lvl3pieceWithDesc.anchor.set(1, 0.5)

            }


            if (langue == 'fr') {
                title.text = "Jeu de Taquin" ;
                titleAR.alpha = 0;
                title.alpha = 1;
                notice.alpha = 1
                noticeAR.alpha = 0
                notice.text = "Merci de faire Glisser les pièces afin de gagner" ;
            }if (langue == 'en') {
                title.text = "Teaser Game" ;
                titleAR.alpha = 0;
                title.alpha = 1;
                notice.alpha = 1
                noticeAR.alpha = 0
                notice.text = "Thanks for dragging coins to win" ;
            }else if (langue == 'ar'){
                titleAR.text = "لعبة اللغز" ;
                titleAR.alpha = 1;
                title.alpha = 0;
                notice.alpha = 0
                noticeAR.alpha = 1
                noticeAR.text = "شكرا لسحب العملات المعدنية للفوز" ;
            }
        },

    }