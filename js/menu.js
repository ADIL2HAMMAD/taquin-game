var menulevels = {
    init: function() {
        me = this

    },
    create: function() {

        isStandBy = localStorage.getItem('standby')
        // prepeare the save Screen
        SetStandBy()

        //Title Arabic and Latin Font Style
        var titlestyleFR = { font: '60px NotoSansExtraCondensedLight', fill: "#f5efe1", align: "center" };
        var titlestyleEN = { font: '60px NotoSansExtraCondensedLight', fill: "#f5efe1", align: "center" };
        var titlestyleAR = { font: '60px NotoArabicExtraCondensedLight', fill: "#f5efe1", align: "center" };

        /* Game background */
        BG = game.add.sprite(game.width / 2, game.height / 2, 'taqinbg')
        BG.anchor.set(0.5, 0.5)
        /*MA Flag  */
        ma_flag = game.add.sprite(BG.width / 2 - 600, BG.height / 2, 'ma_flag')
        ma_flag.anchor.set(0.5, 0.5)
        // AR flag title
        titleAr = game.add.text(ma_flag.x , ma_flag.y - 300  , "إختر لغتك", titlestyleAR);
        titleAr.anchor.set(0.5)
        /*FR Flag  */
        fr_flag = game.add.sprite(BG.width / 2, BG.height / 2, 'fr_flag')
        fr_flag.anchor.set(0.5, 0.5)
        // FR flag title
        titleFR = game.add.text(fr_flag.x , fr_flag.y - 300  , "Choisissez votre langue", titlestyleFR);
        titleFR.anchor.set(0.5)
        // EN flag
        en_flag = game.add.sprite(BG.width / 2 + 600, BG.height / 2, 'en_flag')
        en_flag.anchor.set(0.5, 0.5)
        // EN flag title
        titleEn = game.add.text(en_flag.x , en_flag.y - 300  , "Select your language", titlestyleEN);
        titleEn.anchor.set(0.5)


        ma_flag.inputEnabled = true;
        ma_flag.events.onInputDown.add(function() {
            localStorage.setItem('langue' , 'ar')
            game.state.start('levels')
            savelangue('ar')
        }, this);
        fr_flag.inputEnabled = true;
        fr_flag.events.onInputDown.add(function() {
            localStorage.setItem('langue' , 'fr')
            game.state.start('levels')
            savelangue('fr')
        }, this);
        en_flag.inputEnabled = true;
        en_flag.events.onInputDown.add(function() {
            localStorage.setItem('langue' , 'en')
            game.state.start('levels')
            savelangue('en')
        }, this);

        gameUIenter()
        // excute the ShowSaveScreen Function every second
        game.time.events.loop(Phaser.Timer.SECOND, this.ShowSaveScreen, this);

    },
    // Show save Screen
    ShowSaveScreen:function() {
        isStandBy = localStorage.getItem('standby')
        ShowStandBy(isStandBy)
    },

}


