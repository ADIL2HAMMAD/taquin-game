var loadState = {
    init: function(e) {
        enabled = e;
    },
    create: function() {
        if (game.sound.usingWebAudio &&
            game.sound.context.state === 'suspended') {
            game.input.onTap.addOnce(game.sound.context.resume, game.sound.context);
        }
        root = 'menulevels';
        //  root = 'lvl2';

        game.load.onLoadStart.add(loadStart, this);
        game.load.onFileComplete.add(fileComplete, this);
        game.load.onLoadComplete.addOnce(loadComplete, this);

        start();


        var x = 32;
        var y = 80;

        function start() {


            game.load.spritesheet('puzzlelvl1', 'assets/images/fullpuzzlelvl1.png' , 200 , 200);
            game.load.image('fullpuzzlelvl1', 'assets/images/fullpuzzlelvl1.png');

            game.load.spritesheet('puzzlelvl2', 'assets/images/fullpuzzlelvl2.png' , 150 , 150);
            game.load.image('fullpuzzlelvl2', 'assets/images/fullpuzzlelvl2.png');

            game.load.spritesheet('puzzlelvl3', 'assets/images/fullpuzzlelvl3.png' , 200 , 200);
            game.load.image('fullpuzzlelvl3', 'assets/images/fullpuzzlelvl3.png');

            /* Taqin  SpriteSheet*/
            game.load.image('counterbg', 'assets/images/counterbg.png');
            game.load.image('footer', 'assets/images/footer.png');
            game.load.image('footerlvls', 'assets/images/footerlvls.png');
            game.load.image('taqinbg', 'assets/images/taqinbg.png');
            game.load.image('sidebarbg', 'assets/images/sidebarbg.png');
            game.load.image('ma_flag', 'assets/images/ma_flag.png');
            game.load.image('fr_flag', 'assets/images/fr_flag.png');
            game.load.image('en_flag', 'assets/images/en_flag.png');
            game.load.image('ma_flag_rnd', 'assets/images/ma_flag_rnd.png');
            game.load.image('fr_flag_rnd', 'assets/images/fr_flag_rnd.png');
            game.load.image('en_flag_rnd', 'assets/images/en_flag_rnd.png');
            game.load.image('menu', 'assets/images/menu.png');
            game.load.image('gamebg', 'assets/images/gamebg.png');
            game.load.image('left', 'assets/images/left.png');
            game.load.image('right', 'assets/images/right.png');
            game.load.image('standby', 'assets/images/standby.png');
            game.load.image('hand', 'assets/images/hand_touch.png');

            game.load.image('lvl1', 'assets/images/puzzlelvl1.png');
            game.load.image('lvl2', 'assets/images/puzzlelvl2.png');
            game.load.image('lvl3', 'assets/images/puzzlelvl3.png');

            game.load.image('lvl1piece', 'assets/images/lvl1piece.png');
            game.load.image('lvl2piece', 'assets/images/lvl2piece.png');
            game.load.image('lvl3piece', 'assets/images/lvl3piece.png');




            /* TO REMOVE */

            //PNG
            game.load.image('BlackOverlay', 'assets/images/BlackOverlay.png');
            game.load.image('Restart', 'assets/images/Restart.png');
            game.load.image('Home', 'assets/images/Home.png');;
            game.load.image('Cursor', 'assets/images/cursor.png');
            game.load.image('Loadingframe', 'assets/images/loadingframe.png');
            game.load.image('Loading', 'assets/images/loading.png');
            game.load.image('loadingBG', 'assets/images/loadingBG.png');


            game.load.start();
        }

        function loadStart() {
            loading = game.add.sprite(game.width / 2 - 250, game.height / 2, "loading");
            loading.scale.x = 0;
            loadingframe = game.add.sprite(game.width / 2 - 250, game.height / 2, "loadingframe");

            mytext = game.add.text(game.width / 2, game.height / 2 - 50, '', { align: 'center', font: "60px NotoSansExtraCondensedLight", fill: '#b5aa93' , fontWeight : "bold"  });
            mytext.anchor.set(0.5);
        }

        function fileComplete(progress, cacheKey, success, totalLoaded, totalFiles) {
            loading.scale.x = progress / 100;
            mytext.setText(progress + '%')
        }

        function loadComplete() {
            BlackOverlay = game.add.sprite(0, 0, 'BlackOverlay');
            BlackOverlay.scale.set(500, 200);
            BlackOverlay.alpha = 0;
            game.add.tween(BlackOverlay).to({ alpha: 1 }, 2000, "Linear", true).onComplete.add(function() {
                game.state.start(root);
            });

        }

    }
}