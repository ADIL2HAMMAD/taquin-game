var game = new Phaser.Game(1920, 1080, Phaser.CANVAS, 'game');
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menulevels', menulevels);
game.state.add('levels', levels);
game.state.add('lvl1', level1);
game.state.add('lvl2', level2);
game.state.add('lvl3', level3);

game.state.start('boot');


function SetStandBy(isStandBy) {
    //StandBy Arabic and Latin Font Style
    var standbymsgstyle = { font: '40px NotoSansExtraCondensedLight', fill: "#b5aa93", align: "center" , fontWeight: "bolder" };
    var standbymsgstyleAR = { font: '40px NotoArabicExtraCondensedLight', fill: "#b5aa93", align: "center" , fontWeight: "bolder" };

    //standby background
    standby = game.add.sprite(game.width /2 , game.height/2 , 'standby')
    standby.anchor.set(0.5)
    standby.alpha = 0
    hand = game.add.sprite(standby.x , standby.y + 250 , 'hand')
    hand.anchor.set(0.5)
    hand.scale.setTo(0.8)
    hand.alpha = 0
    standbymsgFR = game.add.text(hand.x , hand.y + 125 , "Toucher l'ecran interactif pour commencer", standbymsgstyle);
    standbymsgFR.anchor.set(0.5)
    standbymsgFR.alpha = 0
    standbymsgEN = game.add.text(hand.x , hand.y + 175 , "Touch the interactive screen to start", standbymsgstyle);
    standbymsgEN.anchor.set(0.5)
    standbymsgEN.alpha = 0
    standbymsgAR = game.add.text(hand.x , hand.y + 225 , " المس الشاشة التفاعلية للبدء", standbymsgstyleAR);
    standbymsgAR.anchor.set(0.5)
    standbymsgAR.alpha = 0

    if (isStandBy == 'true') {
    standby.inputEnabled = true;
    }

    standby.events.onInputDown.add(function() {
        game.state.start('menulevels');
        localStorage.setItem('standby' , false)
        localStorage.setItem('startTime', new Date().toJSON())
    }, this);
}
var x = 0
function ShowStandBy(isStandBy) {

    if (isStandBy == 'true') {
        x++
        if (x == 1) {
            localStorage.setItem('endTime', new Date().toJSON())
            $.ajax({
                url: "http://192.168.0.120:8000/api/session",
                type: "POST",
                data: 'startTime='+ localStorage.getItem('startTime') + '&endTime='+ localStorage.getItem('endTime'),
                success: function (data) {
                    console.log(data);
                }
            })
        }
        standby.alpha = 1
        standby.inputEnabled =true
        hand.alpha = 1
        standbymsgFR.alpha = 1
        standbymsgEN.alpha = 1
        standbymsgAR.alpha = 1
        game.world.bringToTop(standby)
        game.world.bringToTop(hand)
        game.world.bringToTop(standbymsgFR)
        game.world.bringToTop(standbymsgEN)
        game.world.bringToTop(standbymsgAR)

        game.add.tween(hand).to({ alpha: 0.5 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
            game.add.tween(hand).to({ alpha: 1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
            }, this);
        }, this);
    }else if (isStandBy == 'false') {
        x = 0
        localStorage.setItem('standby' , isStandBy)
        standby.inputEnabled = false
        standby.alpha = 0
        hand.alpha = 0
        standbymsgFR.alpha = 0
        standbymsgEN.alpha = 0
        standbymsgAR.alpha = 0
    }
}

function savelangue(language){
    $.ajax({
        url: "http://192.168.0.120:8000/api/language",
        type: "POST",
        data: 'language='+ language,
        success: function (data) {
            console.log(data);
        }
    })
}


function gameUIenter() {


    if (game.state.current == "lvl1" || game.state.current == "lvl2" || game.state.current == "lvl3") {
        Restart = game.add.sprite(60, game.height / 2 + 150, 'Restart');
        Restart.anchor.set(0, 1);
        Restart.inputEnabled = true
        /* Home = game.add.sprite(Restart.width + 60, game.height / 2 - 400, 'Home'); */
        Home = game.add.sprite(60, game.height / 2 - 150, 'Home');
        Home.anchor.set(0, 1);
        Home.inputEnabled = true;
        Restart.events.onInputDown.add(restartfunc, this);
        currentLevel = 1;
        nabled = true;
        Home.events.onInputDown.add(gotoHome, this);
    }


}

function restartfunc() {
    clic = true;
    game.sound.stopAll();
    if (game.state.current == "lvl1") {
        game.state.start("lvl1");
    } else if (game.state.current == "lvl2") {
        game.state.start("lvl2");
    }else if (game.state.current == "lvl3") {
        game.state.start("lvl3");
    }
}

function gotoHome() {
    clic = true;
    game.sound.stopAll();
    game.state.start('levels');
}
