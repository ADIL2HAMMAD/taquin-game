var levels = {
    init: function() {
        me = this

    },
    create: function() {
        langue = localStorage.getItem('langue')

        isStandBy = localStorage.getItem('standby')
        // prepeare the save Screen
        SetStandBy(isStandBy)

        /* SideBar background */
        BG = game.add.sprite(game.width / 2, game.height / 2, 'taqinbg')
        BG.anchor.set(0.5, 0.5)

        // levels piece
        lvlbg1 = game.add.sprite(BG.width / 2 - 600, BG.height / 2, 'lvl1')
        lvlbg1.anchor.set(0.5, 0.5)
        lvlbg2 = game.add.sprite(BG.width / 2, BG.height / 2, 'lvl2')
        lvlbg2.anchor.set(0.5, 0.5)
        lvlbg3 = game.add.sprite(BG.width / 2+600, BG.height / 2, 'lvl3')
        lvlbg3.anchor.set(0.5, 0.5)

        //footer
        footer = game.add.sprite(game.width, game.height, 'footer')
        footer.anchor.set(1)


        // title style latin and arabic
        var titlestyle = { font: '100px NotoSansExtraCondensedLight', fill: "#f5efe1", align: "center" };
        var titlestyleAR = { font: '100px NotoArabicExtraCondensedLight', fill: "#f5efe1", align: "center" };
        // levels style latin and arabic
        var style = { font: '70px NotoSansExtraCondensedLight', fill: "#f5efe1", align: "center" };
        var styleAR = { font: '70px NotoArabicExtraCondensedLight', fill: "#f5efe1", align: "center" };

        title = game.add.text(game.width/2  , 150 , " ", titlestyle);
        titleAR = game.add.text(game.width/2  , 150 , " ", titlestyleAR);
        title.anchor.set(0.5, 0.5)
        titleAR.anchor.set(0.5, 0.5)

        //piece levels description
        lvldesc1 = game.add.text(lvlbg1.x ,lvlbg1.y  + 300 , " ", style);
        lvldesc1ar = game.add.text(lvlbg1.x ,lvlbg1.y  + 300 , " ", styleAR);
        lvldesc1.anchor.set(0.5, 0.5)
        lvldesc1ar.anchor.set(0.5, 0.5)
        lvldesc2 = game.add.text(lvlbg2.x ,lvlbg2.y  + 300 , " ", style);
        lvldesc2ar = game.add.text(lvlbg2.x ,lvlbg2.y  + 300 , " ", styleAR);
        lvldesc2.anchor.set(0.5, 0.5)
        lvldesc2ar.anchor.set(0.5, 0.5)
        lvldesc3 = game.add.text(lvlbg3.x ,lvlbg3.y  + 300 , " ", style);
        lvldesc3ar = game.add.text(lvlbg3.x ,lvlbg3.y  + 300 , " ", styleAR);
        lvldesc3.anchor.set(0.5, 0.5)
        lvldesc3ar.anchor.set(0.5, 0.5)



        if (langue == 'fr') {
            title.text = "Jeu de Taquin" ;
        }if (langue == 'en') {
            title.text = "Teaser Game" ;
        }else if (langue == 'ar'){
            titleAR.text = "لعبة اللغز" ;
        }

        if (langue == 'fr') {
            lvldesc1.text = "Débutant" ;
            lvldesc2.text = "Moyen" ;
            lvldesc3.text = "Pro" ;

        }if (langue == 'en') {
            lvldesc1.text = "Beginner" ;
            lvldesc2.text = "Medium" ;
            lvldesc3.text = "Expert" ;

        }else if (langue == 'ar'){
            lvldesc1ar.text = "مبتدئ";
            lvldesc2ar.text = "متوسط" ;
            lvldesc3ar.text = "خبير" ;

        }


        lvlbg1.inputEnabled = true;
        lvlbg1.events.onInputDown.add(function() {
            game.state.start('lvl1');
        }, this);

        lvlbg2.inputEnabled = true;
        lvlbg2.events.onInputDown.add(function() {
            game.state.start('lvl2');
        }, this);

        lvlbg3.inputEnabled = true;
        lvlbg3.events.onInputDown.add(function() {
            game.state.start('lvl3');
        }, this);

        game.time.events.loop(Phaser.Timer.SECOND, this.savescreen, this);

    },

    savescreen:function () {
        isStandBy = localStorage.getItem('standby')
        ShowStandBy(isStandBy)
    }

}