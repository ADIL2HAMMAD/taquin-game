var level1 = {
    //level 2 lunching screen
    create: function() {

        isStandBy = localStorage.getItem('standby')
        // prepeare the save Screen
        SetStandBy(isStandBy)

        langue = localStorage.getItem('langue')
        me = this
        shuffledIndexArray = []
        finished = false
        second = 0
        minute = 0
        hours  = 0

        //Title Arabic and Latin Font Style
        var titlestyle = { font: '70px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" };
        var titlestyleAR = { font: '70px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" };
        //Notice Arabic and Latin Font Style
        var noticestyle = { font: '50px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };
        var noticestyleAR = { font: '50px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };


        /* Game background */
        bg = game.add.sprite(game.width / 2, game.height / 2, 'taqinbg')
        bg.anchor.set(0.5, 0.5)

        /* SideBar background */
        sidebarbg = game.add.sprite(game.width , game.height / 2, 'sidebarbg')
        sidebarbg.anchor.set(1, 0.5)

        //footer
        footer = game.add.sprite(0 , game.height, 'footerlvls')
        footer.anchor.set(0, 1)

        //SideBar Message -Notice-
        notice = game.add.text(sidebarbg.x - 240, sidebarbg.y   , " ", noticestyle);
        notice.anchor.set(0.5)
        noticeAR = game.add.text(sidebarbg.x - 240, sidebarbg.y   , " ", noticestyleAR);
        noticeAR.anchor.set(0.5)

        if (langue == 'fr') {
            notice.text = "Merci de faire Glisser les pièces afin de gagner" ;
        }if (langue == 'en') {
            notice.text = "Thanks for dragging coins to win" ;
        }else if (langue == 'ar'){
            noticeAR.text = "شكرا لسحب العملات المعدنية للفوز" ;
        }

        /* Flags  */
        ma_flag = game.add.sprite(sidebarbg.x - 430 , sidebarbg.y + 450  , 'ma_flag_rnd')
        ma_flag.anchor.set(0)
        ma_flag.scale.setTo(0.65);

        fr_flag = game.add.sprite(sidebarbg.x - 280, sidebarbg.y  + 450 , 'fr_flag_rnd')
        fr_flag.anchor.set(0)
        fr_flag.scale.setTo(0.65);

        en_flag = game.add.sprite(sidebarbg.x  - 130 , sidebarbg.y  + 450 , 'en_flag_rnd')
        en_flag.anchor.set(0)
        en_flag.scale.setTo(0.65);

        // game title
        title = game.add.text(sidebarbg.x - 400 , sidebarbg.y - 525 , " ", titlestyle);
        titleAR = game.add.text(sidebarbg.x - 350 , sidebarbg.y - 525 , " ", titlestyleAR);

        if (langue == 'fr') {
            title.text = "Jeu de Taquin" ;
        }if (langue == 'en') {
            title.text = "Teaser Game" ;
        }else if (langue == 'ar'){
            titleAR.text = "لعبة اللغز" ;
        }

        //white puzzle background        //white puzzle background
        gamebg = game.add.sprite( 345  ,  game.height / 2 - 390, 'gamebg')
        gamebg.anchor.set(0)

        // final puzzle image
        fullpuzzlelvl1 = game.add.sprite( gamebg.x  + 365 , gamebg.y  + 360 , 'fullpuzzlelvl1')
        fullpuzzlelvl1.anchor.set(0.5)
        fullpuzzlelvl1.alpha = 0
        fullpuzzlelvl1.inputEnabled = false;

        // enabling flags click
        ma_flag.inputEnabled = true;
        ma_flag.events.onInputDown.add(function() {
            this.ChangeLanguage('ar');
            savelangue('ar')
        }, this);
        fr_flag.inputEnabled = true;
        fr_flag.events.onInputDown.add(function() {
            this.ChangeLanguage('fr');
            savelangue('fr')
        }, this);
        en_flag.inputEnabled = true;
        en_flag.events.onInputDown.add(function() {
            this.ChangeLanguage('en');
            savelangue('en')
        }, this);



        this.prepareBoard()

        gameUIenter()

    },
    // create a game puzzle story
    prepareBoard: function() {
        var piecesIndex = 0,
        i=0 ;
        j=0 ;
        piece = null;
        PIECE_WIDTH = 200;
        PIECE_HEIGHT = PIECE_WIDTH;

        PADDING_X = 65;
        PADDING_Y = 65;

        BOARD_COLS = 3;
        BOARD_ROWS = 3;

        piecesAmount = BOARD_COLS * BOARD_ROWS;

        shuffledIndexArray = this.createShuffledIndexArray();

        piecesGroup = game.add.group();

        for (i = 0; i < BOARD_ROWS; i++)
        {
            for (j = 0; j < BOARD_COLS; j++)
            {
                if (shuffledIndexArray[piecesIndex]) {
                    piece = piecesGroup.create( (gamebg.x + PADDING_X) + j * PIECE_WIDTH ,(gamebg.y + PADDING_Y) +  i * PIECE_HEIGHT , "puzzlelvl1", shuffledIndexArray[piecesIndex]);
                }
                else{ //initial position of black piece
                    piece = piecesGroup.create((gamebg.x + PADDING_X) +  j * PIECE_WIDTH , (gamebg.y + PADDING_Y) +  i * PIECE_HEIGHT);
                    piece.black = true;
                }
                piece.anchor.set(0)
                piece.name = 'piece' + i.toString() + 'x' + j.toString();
                piece.currentIndex = piecesIndex;
                piece.destIndex =  shuffledIndexArray[piecesIndex]; // shuffledIndexArray[piecesIndex] ==> Frame ID from Source Image and Final distination frame ID
                piece.inputEnabled = true;
                piece.events.onInputDown.add(this.selectPiece, this);
                piece.posX = j;
                piece.posY = i;
                piecesIndex++;

            }
        }
        // counter background
        counterbg = game.add.sprite(fr_flag.x + 30 , fr_flag.y  - 100, 'counterbg')
        counterbg.anchor.set(0.5)
        // left Arrow -PREVIOUS-
        left = game.add.sprite(counterbg.x - 150 , counterbg.y, 'left')
        left.anchor.set(0.5)
        left.scale.setTo(0.7);
        left.alpha = 0.2

        // right Arrow -NEXT-
        right = game.add.sprite(counterbg.x + 150 , counterbg.y , 'right')
        right.anchor.set(0.5)
        right.scale.setTo(0.7);
        right.alpha = 0.2

        //counter
        counter = game.add.text(fr_flag.x - 25 , fr_flag.y  - 125, '0 : 0 : 0', { font: "40px NotoSansExtraCondensedLight", fill: "#ffffff", align: "center" , fontWeight : "bold" });
        game.time.events.loop(Phaser.Timer.SECOND, this.updateCounter, this);

    },
    // move piece if a white piece in neighborhood
    selectPiece: function(piece) {

        var blackPiece = this.canMove(piece);
        //if there is a black piece in neighborhood
        if (blackPiece) {
            this.movePiece(piece, blackPiece);
        }

    },
    // check if we can move the piece
    canMove: function(piece) {
        var foundBlackElem = false;

        piecesGroup.children.forEach(function(element) {
            if (element.posX === (piece.posX - 1) && element.posY === piece.posY && element.black ||
            element.posX === (piece.posX + 1) && element.posY === piece.posY && element.black ||
            element.posY === (piece.posY - 1) && element.posX === piece.posX && element.black ||
            element.posY === (piece.posY + 1) && element.posX === piece.posX && element.black) {
                foundBlackElem = element;
                return;
            }
        });

        return foundBlackElem;
    },
    // Move the piece to the white piece
    movePiece: function(piece, blackPiece) {

        var tmpPiece = {
            posX: piece.posX,
            posY: piece.posY,
            currentIndex: piece.currentIndex
        };

        game.add.tween(piece).to({x: (gamebg.x + PADDING_X) + blackPiece.posX * PIECE_WIDTH, y: (gamebg.y + PADDING_Y) + blackPiece.posY * PIECE_HEIGHT}, 300, Phaser.Easing.Linear.None, true);

        //change places of piece and blackPiece
        piece.posX = blackPiece.posX;
        piece.posY = blackPiece.posY;
        piece.currentIndex = blackPiece.currentIndex;
        piece.name ='piece' + piece.posX.toString() + 'x' + piece.posY.toString();

        //piece is the new black
        blackPiece.posX = tmpPiece.posX;
        blackPiece.posY = tmpPiece.posY;
        blackPiece.currentIndex = tmpPiece.currentIndex;
        blackPiece.name ='piece' + blackPiece.posX.toString() + 'x' + blackPiece.posY.toString();

        //after every move check if puzzle is completed
        this.checkIfFinished();
    },
    // Update Counter
    updateCounter:function() {
        if (!finished) {
            second++ ;
            if (second == 60) {
                second = 0
                minute++
            }
            if (minute == 60) {
                second = 0
                minute = 0
                hours++
            }
            counter.setText(hours + ' : ' + minute + ' : ' + second);
        }else{
            game.add.tween(fullpuzzlelvl1.scale).to({ x: 1.1, y: 1.1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                game.add.tween(fullpuzzlelvl1.scale).to({ x: 1, y: 1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                }, this);
            }, this);


            counter.alpha = 1;
            game.add.tween(counter).to({ alpha: 0.5 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                game.add.tween(counter).to({ alpha: 1 }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                }, this);
            }, this);
        }
        isStandBy = localStorage.getItem('standby')
        ShowStandBy(isStandBy)
    },
    // to check if player is Wining the level
    checkIfFinished : function() {
        var isFinished = true;

        piecesGroup.children.forEach(function(element) {
            // element.destIndex = 1;
            // element.currentIndex =1 ;

            if (element.currentIndex !== element.destIndex) {
                isFinished = false;
                return;
            }
        });

        if (isFinished) {
            //title Piece Arabic and Latin Font Style
            piecetitlestyle = { font: '27px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" , fontWeight: "Bold"  };
            piecetitlestyleAR = { font: '27px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" , fontWeight: "Bold" };
            //Piece Arabic and Latin Font Style
            piecetextstyle = { font: '22px NotoSansExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };
            piecetextstyleAR = { font: '22px NotoArabicExtraCondensedLight', fill: "#1c1c1c", align: "center" , wordWrap: true, wordWrapWidth: 400  };


            piecesGroup.children.forEach(function(element) {
                element.alpha = 0;
            });
            // Picture to display while if player wining level
            lvl1piece = game.add.sprite(sidebarbg.x - 240, sidebarbg.y - 300  , 'lvl1piece');
            lvl1piece.anchor.set(0.5)


            titlepiece = game.add.text(lvl1piece.x , lvl1piece.y + 150  , "", piecetitlestyle);
            titlepiece.anchor.set(0.5)

            titlepieceAR = game.add.text(lvl1piece.x , lvl1piece.y + 150  , "", piecetitlestyleAR);
            titlepieceAR.anchor.set(0.5)

            piecetext = game.add.text(titlepiece.x , titlepiece.y + 200   , "", piecetextstyle);
            piecetext.anchor.set(0.5)

            piecetextAR = game.add.text(titlepiece.x , titlepiece.y + 200   , "", piecetextstyleAR);
            piecetextAR.anchor.set(0.5)


            // piece with title and description translation first display while winning
            if (langue == 'fr') {
                titlepiece.text = "Aureus de Juba II,vers 19 av. J.-C 3,61 g, 15,5 mm"
                piecetext.text = "Elevé dans un milieu privilégié à Rome, Juba II (25 av. J.-C. - 23 J.-C.) a pu acquérir une culture gréco-latine avant de devenir roi de Maurétanie. Ce souverain érudit épousa la princesse égyptienne Cléopâtre Séléné et célébra cette alliance par l’émission vers 19 av. J.-C. d’un aureus en or qui porte à l’avers son effigie et sa titulature en latin : REX IVBA et au revers le symbole égyptien d’Isis accompagné du nom de Cléopâtre en grec." ;
            }if (langue == 'en') {
                titlepiece.text = "Juba II (25 av. -23 AD.) around 19 BC 3,61 g, 15,5 mm"
                piecetext.text = "Raised in a privileged environment in Rome, Juba II (25 BC - 23 AD) managed to acquire a Greco-Latin culture before he became King of Mauritania. This erudite ruler married the Egyptian princess Cleopatra Selene and celebrated this alliance by issuing a gold aureus around 19 BC with his effigy of the king and his latin titulature on the obverse: REX IVBA and the egyptian symbol of Isis with the name Cleopatra in Greek on the reverse." ;
            }else if (langue == 'ar'){
                titlepieceAR.text = " ‫جوبا ‬الثاني‬ ‫أوريوس‬ .‫م‬ .‫ق‬ 19 ‫حوالي‬ .‫ملم‬ 15,5 ،‫غ‬ 3,61"
                piecetextAR.text = "قبل‬ ‫واسعة‬ ‫ولاتينية‬ ‫يونانية‬ ‫ثقافة‬ ‫اكتساب‬ ‫من‬ ‫وتمكن‬ ،‫روما‬ ‫في‬ ‫متميز‬ ‫محيط‬ ‫في‬ (‫م‬ 23 - ‫م‬ .‫ق‬ 25) ‫الثاني‬ ‫جوبا‬ ‫نشأ‬ قطعة‬ ‫جوبا‬ ‫الملك‬ ‫أصدر‬ ،‫سيليني‬ ‫كليوباترا‬ ‫المصرية‬ ‫بالأميرة‬ ‫بزواجه‬ ‫احتفاله‬ ‫وبمناسبة‬ .‫موريطانيا‬ ‫مملكة‬ ‫عرش‬ ‫اعتلائه‬ اللاتينية‬ ‫باللغة‬ ‫مكتوب‬ ‫لقبه‬ ‫مع‬ ‫الملك‬ ‫بصورة‬ ‫الوجه‬ ‫على‬ ‫مزينة‬ ‫القطعة‬ ‫هذه‬ ‫وكانت‬ ،‫م‬ .‫ق‬ 19 ‫سنة‬ ‫الذهبية‬ ‫الأوريوس‬ .‫اليونانية‬ ‫باللغة‬ ‫كليوباترا‬ ‫باسم‬ ‫مرفوقا‬ ‫إيزيس‬ ‫للإلهة‬ ‫المصري‬ ‫الرمز‬ ،‫الظهر‬ ‫وعلى‬ (REX IVBA)"
            }

            //Piece With Description at Finished
            notice.alpha = 0
            noticeAR.alpha = 0
            // piecetextAR.alpha = 0

            finished = true;
            fullpuzzlelvl1.alpha = 1




            right.alpha = 1

            game.add.tween(right).to({ alpha: .5 }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                game.add.tween(right).to({ alpha: 1 }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.add(function() {
                })
            })


            right.inputEnabled = true;
            right.events.onInputDown.add(function() {
                game.state.start('lvl2')
            }, this);


        }

    },
    // create an random array
    createShuffledIndexArray: function() {
        var indexArray = [];

        for (var i = 0; i < piecesAmount; i++)
        {
            indexArray.push(i);
        }

        return this.shuffle(indexArray);

    },
    // transform an order array to random
    shuffle: function(array) {
        var counter = array.length;
        temp = null;
        index = 0;

        while (counter > 0)
        {
            index = Math.floor(Math.random() * counter);

            counter--;

            temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }

        return array;

    },
    // changing game language
    ChangeLanguage : function(langue ){
        localStorage.setItem('langue' , langue);

        if (finished) {
            notice.alpha = 0
            noticeAR.alpha = 0
            // piece with title and description translation while changing language
            if (langue == 'fr') {
                titlepiece.text = "Aureus de Juba II,vers 19 av. J.-C 3,61 g, 15,5 mm"
                piecetext.text = "Elevé dans un milieu privilégié à Rome, Juba II (25 av. J.-C. - 23 J.-C.) a pu acquérir une culture gréco-latine avant de devenir roi de Maurétanie. Ce souverain érudit épousa la princesse égyptienne Cléopâtre Séléné et célébra cette alliance par l’émission vers 19 av. J.-C. d’un aureus en or qui porte à l’avers son effigie et sa titulature en latin : REX IVBA et au revers le symbole égyptien d’Isis accompagné du nom de Cléopâtre en grec." ;
                titleAR.alpha = 0
                title.alpha = 1
                titlepieceAR.alpha = 0
                piecetextAR.alpha = 0
                titlepiece.alpha = 1
                piecetext.alpha = 1
            }if (langue == 'en') {
                titlepiece.text = "Juba II (25 av. -23 AD.) around 19 BC 3,61 g, 15,5 mm"
                piecetext.text = "Raised in a privileged environment in Rome, Juba II (25 BC - 23 AD) managed to acquire a Greco-Latin culture before he became King of Mauritania. This erudite ruler married the Egyptian princess Cleopatra Selene and celebrated this alliance by issuing a gold aureus around 19 BC with his effigy of the king and his latin titulature on the obverse: REX IVBA and the egyptian symbol of Isis with the name Cleopatra in Greek on the reverse." ;
                titleAR.alpha = 0
                title.alpha = 1
                titlepieceAR.alpha = 0
                piecetextAR.alpha = 0
                titlepiece.alpha = 1
                piecetext.alpha = 1
            }else if (langue == 'ar'){
                titlepieceAR.text = " ‫جوبا ‬الثاني‬ ‫أوريوس‬ .‫م‬ .‫ق‬ 19 ‫حوالي‬ .‫ملم‬ 15,5 ،‫غ‬ 3,61"
                piecetextAR.text = "قبل‬ ‫واسعة‬ ‫ولاتينية‬ ‫يونانية‬ ‫ثقافة‬ ‫اكتساب‬ ‫من‬ ‫وتمكن‬ ،‫روما‬ ‫في‬ ‫متميز‬ ‫محيط‬ ‫في‬ (‫م‬ 23 - ‫م‬ .‫ق‬ 25) ‫الثاني‬ ‫جوبا‬ ‫نشأ‬ قطعة‬ ‫جوبا‬ ‫الملك‬ ‫أصدر‬ ،‫سيليني‬ ‫كليوباترا‬ ‫المصرية‬ ‫بالأميرة‬ ‫بزواجه‬ ‫احتفاله‬ ‫وبمناسبة‬ .‫موريطانيا‬ ‫مملكة‬ ‫عرش‬ ‫اعتلائه‬ اللاتينية‬ ‫باللغة‬ ‫مكتوب‬ ‫لقبه‬ ‫مع‬ ‫الملك‬ ‫بصورة‬ ‫الوجه‬ ‫على‬ ‫مزينة‬ ‫القطعة‬ ‫هذه‬ ‫وكانت‬ ،‫م‬ .‫ق‬ 19 ‫سنة‬ ‫الذهبية‬ ‫الأوريوس‬ .‫اليونانية‬ ‫باللغة‬ ‫كليوباترا‬ ‫باسم‬ ‫مرفوقا‬ ‫إيزيس‬ ‫للإلهة‬ ‫المصري‬ ‫الرمز‬ ،‫الظهر‬ ‫وعلى‬ (REX IVBA)"
                titleAR.alpha = 1
                title.alpha = 0
                titlepiece.alpha = 0
                piecetext.alpha = 0
                titlepieceAR.alpha = 1
                piecetextAR.alpha = 1
            }
        }
        else{
            // Notice content while changing language
            if (langue == 'fr') {
                notice.alpha = 1
                noticeAR.alpha = 0
                notice.text = "Merci de faire Glisser les pièces afin de gagner" ;
            }if (langue == 'en') {
                notice.alpha = 1
                noticeAR.alpha = 0
                notice.text = "Thanks for dragging coins to win" ;
            }else if (langue == 'ar'){
                notice.alpha = 0
                noticeAR.alpha = 1
                noticeAR.text = "شكرا لسحب العملات المعدنية للفوز" ;
            }
        }
        // Title of the game while changing language
        if (langue == 'fr') {
            title.text = "Jeu de Taquin" ;
            titleAR.alpha = 0;
            title.alpha = 1
        }if (langue == 'en') {
            title.text = "Teaser Game" ;
            titleAR.alpha = 0;
            title.alpha = 1
        }else if (langue == 'ar'){
            titleAR.text = "لعبة اللغز" ;
            titleAR.alpha = 1;
            title.alpha = 0
        }
    }


}