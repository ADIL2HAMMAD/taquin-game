
var bootState = {
    init:function(e){
        etat = e;
    },
    preload: function(){

        // Existing code unchanged.

        game.input.onTap.addOnce(game.sound.context.resume, game.sound.context);

        game.load.image('loadingBG', 'assets/images/loadingBG.png');
        game.load.image('loading', 'assets/images/loading.png');
        game.load.image('loadingframe', 'assets/images/loadingframe.png');
        game.stage.backgroundColor = "#fff";
        function setupScaling() {
            if (game.device.desktop) {
                game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
                game.scale.pageAlignHorizontally = true;
                game.scale.pageAlignVertically = true;
                //game.scale.setScreenSize(true);
            } else {
                game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
                game.scale.pageAlignHorizontally = true;
                game.scale.pageAlignVertically = true;
                game.scale.forceOrientation(false, true, 'orientation');
            }
            game.world.setBounds(0, 0, 798, 456);
        }
        setupScaling();

    } ,

    create: function (){
        game.state.start('load');
        game.add.text(0, 0, "hack", {font:"1px NotoSansExtraCondensedLight", fill:"#FFFFFF"});
        game.add.text(0, 0, "hack", {font:"1px NotoArabicExtraCondensedLight", fill:"#FFFFFF"});

    }
}

