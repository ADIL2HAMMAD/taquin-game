# Jeu du Taquin

***Le taquin*** est un jeu solitaire en forme de damier créé vers 1870 aux États-Unis. Sa théorie mathématique a été publiée par l'American Journal of mathematics pure and appliqué en 1879. En 1891, son invention fut revendiquée par Sam Loyd3, au moment où le jeu connaissait un engouement considérable, tant aux États-Unis qu'en Europe. Il est composé de 15 petits carreaux numérotés de 1 à 15 qui glissent dans un cadre prévu pour 16. Il consiste à remettre dans l'ordre les 15 carreaux à partir d'une configuration initiale quelconque.

Le principe a été étendu à toutes sortes d'autres jeux. La plupart sont à base de blocs rectangulaires plutôt que carrés, mais le but est toujours de disposer les blocs d'une façon déterminée par un nombre minimal de mouvements. **Le Rubik's** Cube est aujourd'hui considéré comme l'un des « descendants » du taquin.

***Objectif :*** manipuler et se familiariser avec des monnaies de différentes époques.

Il s’agit d’une version interactive du jeu de taquin qui consiste à reconstituer une image fragmentée en déplaçant par glissement les éléments qui la composent. Pour cela une case est laissée vide et le joueur doit compléter l’image avec le moins de mouvement possible.

Dans cette version, une fois reconstituée l’image, le joueur aura accès à une description complète de la monnaie ou du billet. ainsi qu’à un petit texte d’approfondissement de ces émissions maîtresses.

Les pièces et billets retenues seront des émissions exposées dans le musée et importantes pour comprendre l’histoire de la monnaie marocaine.

11 tableaux sont à produire, avec des niveaux de difficulté variables de 3*3 cases pour les plus petits à 5*5 cases pour les plus grands.

Exemples d’émissions utilisées: Aureus de JubaII, 10dinars « Moqtada , coupure de 10 000francs surchargé 100dirhams...

## Installation

Déplacer le dossier après le télécharger ou clone dans XAMPP ou un serveur équivalant , ensuite lancer le navigateur vers le lien suivant :  **http://localhost/puzzle/game.html**

## Technologie Utilisée

**Phaser** est un framework de jeu gratuit en 2D permettant de créer des jeux HTML5 pour ordinateurs de bureau et mobiles. Il est développé par Photon Storm.

Phaser utilise un moteur de rendu Canvas et WebGL en interne et peut basculer automatiquement entre eux en fonction de la prise en charge du navigateur. Cela permet un rendu rapide sur les ordinateurs de bureau et mobiles. Il utilise la bibliothèque Pixi.js pour le rendu.

Les jeux peuvent être compilés pour iOS, Android et les applications de bureau natives via des outils tiers tels qu'Apache Cordova.

La seule condition requise pour utiliser Phaser est un navigateur Web prenant en charge la balise HTML <canvas>. Pour le bureau, cela inclut Chrome, Firefox, Safari, IE9 + et Opera. Le développement peut être effectué en JavaScript ou en TypeScript .

Certains des éléments disponibles sont:

* Images, feuilles de sélection et tweens - Images statiques et dynamiques, ainsi qu'un mécanisme pour les animer.
* Contrôle d'entrée
* Physique du jeu - Phaser comprend trois principaux moteurs physiques.

## Architecture et Fonctionnalités

Les jeux réalisés avec phaser sont développés en JavaScript ou en TypeScript.

Le jeu est rendu sur un élément WebGL ou Canvas.

Phaser prend en charge Spritesheet, chargement des sprites; qui peut être utilisé pour animer, déplacer, etc. Il prend également en charge le chargement de cartes en mosaïque.

### Le rendu
Phaser peut être rendu dans WebGl ou dans un canevas, avec l'option d'utiliser WebGL si le navigateur le prend en charge ou si un périphérique ne le prend pas en charge, il revient à Canvas.

### La physique
Phaser est livré avec le système Arcade Physics, Ninja Physics et P2.JS, un système de physique complet du corps.

### Animation et audio
L'animation peut être réalisée en phaser en chargeant une feuille de calcul, un atlas de texture et en créant une séquence d'animation. L’audio Web et l’audio HTML5 peuvent être utilisés pour lire le son dans Phaser.

### Scripting
Les jeux Phaser peuvent être créés en JavaScript ou en typographie.
